export class Ticket {
    category: number;
    comment: string;
    sucursal: number;
    user_employee: number;
    priority: number;

    constructor() {
        this.category = 0
        this.sucursal = 0
        this.priority = 0
    }
}