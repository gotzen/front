

import { Component } from '@angular/core';
import { Service } from "app/services/service";
import { ServiceUser } from "app/services/serviceUser";

@Component({
    selector: 'clientecomponent',
    templateUrl: './listticket.component.html'
})
export class ListTicketComponent {
    list = []
    title = 'hola'
    constructor(private _service: Service) {

    }
    ngOnInit() {
        let obj = {};
        obj["user_employee"] = ServiceUser.getUserId();
        this._service.post("http://localhost:8000/ticket/api-list-ticket", obj).then(
            (res) => {
                if (res.error == null) {
                    console.log(res.ret)
                    this.list = res.ret;
                }
            }
        )
    }

}