
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, ResponseContentType } from '@angular/http';



@Injectable()
export class Service {

    private _headers = new Headers;
    private _headerFile = new Headers;

    constructor(private _http: Http) {
        this._headers.append("Content-type", "application/json");
        this._headerFile.append("enctype", "multipart/form-data");

    }


    get(ruta, params?
    ): Promise<any> {

        return new Promise((resolve, reject) => {
            this._http.get(
                ruta + "/" + params

            ).subscribe(
                res => {

                    resolve(res.json());
                },
                error => {

                    reject(error.json());
                }
                )
        })
    }
    Post() {
        var Httpreq = new XMLHttpRequest(); // a new request
        Httpreq.open("POST", 'http://127.0.0.1:8000/ticket/api-list-category', false);
        Httpreq.send(null);
        return Httpreq.responseText;
    }




    /**
     *  Se utiliza para hacer llamdas get a un servicio determinado con el verbo post    
     */
    post(ruta, body): Promise<any> {

        return new Promise((resolve, reject) => {

            console.log(ruta);
            this._http.post(ruta, this.parseJson(body),
                {
                    headers: this._headers
                }
            ).subscribe(
                res => {
                    console.log(res);
                    resolve(res.json());
                },
                err => {
                    reject(err.json());
                }
                )
        })
    }
    private parseJson(body) {

        return JSON.stringify(body);
    }
}



