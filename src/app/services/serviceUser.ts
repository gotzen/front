import { Injectable } from '@angular/core';
@Injectable()
export class ServiceUser {
    private static idUser;
    private nombre;
    private apellido;
    private rol;


    public static getUserId() {
        return this.idUser;
    }
    public static setUserId(user_id) {
        this.idUser = user_id;
    }
}