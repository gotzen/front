import { Component } from '@angular/core';
import { Service } from "app/services/service";
import { Router } from "@angular/router";

@Component({
    selector: 'menu-component',
    templateUrl: './menu.component.html'
})
export class MenuComponent {

    title = 'hola'
    constructor(private _service: Service, private router: Router) {

    }

    // login(){
    //     this._service.post("http://localhost:8000/ticket/api/login").then(
    //         (res)=>{
    //             if(res.)
    //         }
    //     )
    // }

    goClientes() {
        this.router.navigate(['/menu/cliente'])
    }
    goTicket() {
        this.router.navigate(['/menu/createticket'])
    }
    goListTicket() {
        this.router.navigate(['/menu/list-ticket'])
    }
}