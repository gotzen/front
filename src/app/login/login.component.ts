import { Component } from '@angular/core';
import { Service } from "app/services/service";
import { User } from "app/models/user";
import { ServiceUser } from "app/services/serviceUser";
import { Router } from "@angular/router";
import { ToasterService } from "angular2-toaster/angular2-toaster";

@Component({
    selector: 'logincomponent',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    user: User = new User();
    title = 'hola'
    private toasterService: ToasterService;

    constructor(private _service: Service, private router: Router, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    login() {
        this._service.post("http://localhost:8000/ticket/api-login", this.user).then(
            (res) => {
                console.log(res)
                if (res.error == null) {
                    ServiceUser.setUserId(res.ret.idUser);
                    this.toasterService.pop('success', 'Login', "Conexion Existosa");
                    this.router.navigate(['/menu/cliente'])
                } else {

                    this.toasterService.pop('error', 'Validación', res.error.mensaje);
                    this.user = new User();
                }
            }
        )
    }
}