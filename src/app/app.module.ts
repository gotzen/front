import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from "app/login/login.component";
import { routing } from "app/app.routing";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Service } from "app/services/service";
import { HttpModule } from "@angular/http";
import { MenuComponent } from "app/menu/menu.component";
import { ClienteComponent } from "app/cliente/cliente.component";
import { CreateTicketComponent } from "app/ticket/createticket.component";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { ListTicketComponent } from "app/listticket/listticket.component";
import { ServiceUser } from "app/services/serviceUser";
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    ClienteComponent,
    CreateTicketComponent,
    ListTicketComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ToasterModule

  ],
  providers: [Service, ServiceUser],
  bootstrap: [AppComponent]
})
export class AppModule { }
