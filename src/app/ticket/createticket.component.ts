import { Component } from '@angular/core';
import { Service } from "app/services/service";
import { Router } from "@angular/router";
import { Ticket } from "app/models/ticket";
import { ServiceUser } from "app/services/serviceUser";

@Component({
    selector: 'createticket-component',
    templateUrl: './createticket.component.html'
})
export class CreateTicketComponent {
    ticket: Ticket = new Ticket();
    title = 'hola'
    listCategory = [];
    listSucursal = [];
    constructor(private _service: Service, private router: Router) {

    }
    ngOnInit() {
        let obj = {};
        obj["cualquier"] = "hola";
        this._service.post("http://127.0.0.1:8000/ticket/api-list-category", obj).then(
            (res) => {
                if (res.error == null) {
                    this.listCategory = res.ret;
                    console.log(this.listCategory);
                }
            }
        )

        obj["cualquier"] = "hola";
        this._service.post("http://127.0.0.1:8000/ticket/api-list-sucursal", obj).then(
            (res) => {
                if (res.error == null) {
                    this.listSucursal = res.ret;

                }
            }
        )
    }

    save() {
        this.ticket.user_employee = ServiceUser.getUserId();
        this._service.post("http://127.0.0.1:8000/ticket/api-create-ticket", this.ticket).then(
            (res) => {
                console.log(res);
            }
        )
    }


}