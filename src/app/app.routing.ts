import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "app/login/login.component";
import { MenuComponent } from "app/menu/menu.component";
import { ClienteComponent } from "app/cliente/cliente.component";
import { CreateTicketComponent } from "app/ticket/createticket.component";
import { ListTicketComponent } from "app/listticket/listticket.component";

export const router: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    {
        path: 'menu', component: MenuComponent, children:
        [
            { path: 'cliente', component: ClienteComponent },
            { path: 'list-ticket', component: ListTicketComponent },
            { path: 'createticket', component: CreateTicketComponent }

        ]
    }
];



export const appRoutingProviders: any[] = [];
export const routing = RouterModule.forRoot(router);
